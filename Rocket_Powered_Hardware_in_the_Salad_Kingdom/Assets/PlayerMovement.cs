﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{

    public float maxSpeed = 5f;

    float BoundaryRadius = 0.5f;

    void Start()
    {

    }

    void Update()
    {
        LookTowards(Input.mousePosition);

        // MOVE the dude.
        Vector3 pos = transform.position;
        Vector3 velocity = new Vector3(Input.GetAxis("Horizontal") * maxSpeed * Time.deltaTime, Input.GetAxis("Vertical") * maxSpeed * Time.deltaTime, 0);
        pos += velocity;

        // Finally, update our position!!
        transform.position = pos;
        Camera.main.transform.position = new Vector3(pos.x, pos.y, -10f);
    }

    void LookTowards(Vector3 target)
    {
        Vector3 object_pos = Camera.main.WorldToScreenPoint(transform.position);
        float angle = Mathf.Atan2(target.y - object_pos.y, target.x - object_pos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }
}
