﻿using UnityEngine;
using System.Collections;

public class Seek : MonoBehaviour {

    public float maxSpeed = 5.0f;
    public GameObject Player;
    private Vector3 playerLoc;
	// Use this for initialization
	void Start () {
        playerLoc = Player.GetComponent<Transform>().position;
    }
	
	// Update is called once per frame
	void Update () {
        LookTowards(playerLoc);
    }

    void LookTowards(Vector3 target)
    {
        Vector3 object_pos = transform.position;
        float angle = Mathf.Atan2(target.y - object_pos.y, target.x - object_pos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }

    void MoveTowards(Vector3 target)
    {
        // MOVE the dude.
        Vector3 pos = transform.position;
        Vector3 object_pos = Camera.main.WorldToScreenPoint(transform.position);
        float angle = Mathf.Atan2(target.y - object_pos.y, target.x - object_pos.x) * Mathf.Rad2Deg;
        Vector3 velocity = new Vector3(maxSpeed * Time.deltaTime, maxSpeed * Time.deltaTime, angle);
        pos += velocity;
    }
}
